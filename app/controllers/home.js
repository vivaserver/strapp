module.exports = []
  .concat({
    path: "/",
    method: "GET",
    handler: (request, h) => {
      return h.view("home")
    }
  })
  .concat({
    path: "/moonfolio/{page}",
    method: "GET",
    handler: (request, h) => {
      return h.view(`moonfolio/${request.params.page}`)
    }
  })
  .concat({
    path: "/bfc/{page}",
    method: "GET",
    handler: (request, h) => {
      return h.view(`bfc/${request.params.page}`)
    }
  })
  .concat({
    path: "/stack/{page}",
    method: "GET",
    handler: (request, h) => {
      return h.view(`stack/${request.params.page}`)
    }
  })

// vim: nofoldenable
